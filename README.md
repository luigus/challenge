# BairesDev - Code Challenge

### Techs

* [Python 3.7] - Main language
* [Pipenv] - Python package installer for pipenv
* [requirements.txt] - File with the libraries used in the project for virtualenv
* [Pipfile] - File with the libraries used in the project for pipenv

All the code is available in a private Bitbucket repository.


### Clone project

```sh
$ git clone git@bitbucket.org:luigus/challenge.git
```

### Preparing the virtual environment with pipenv
```sh
$ cd challenge/source
$ mkdir .venv
$ pipenv --python 3.7
$ pipenv shell
$ pipenv install
```

### Installing dependencies with Pipenv
```sh
$ pipenv shell
$ pipenv install
```

### Preparing the virtual environment with virtualenv
```sh
$ cd challenge/source
$ virtualenv .venv
$ source .venv/bin/activate
```

### Installing dependencies with virtualenv
```sh
$ pip install -r requirements.txt
```

### How to run the project locally - Task 1
Need to pass the list of Form Numbers in quotes, separated by one space, as shown below
```sh
$ cd challenge/source/src
$ python task1.py "Publ 17" "Publ 527"
```
The program will show the output in json format:
```
$ [{"form_number": "Publ 17", "form_title": "Your Federal Income Tax (For Individuals)", "min_year": 1994, "max_year": 2020},
 {"form_number": "Publ 527", "form_title": "Residential Rental Property (Including Rental of Vacation Homes)", "min_year": 1994, "max_year": 2020}
]
```

### How to run unit tests - Task 1
```sh
$ cd challenge/source/
$ python -m unittest tests/test_task1.py
```


### How to run the project locally - Task 2
Need to pass one Form Number in quotes separated by one space, and minimum year and maximum year range as shown below
```sh
$ cd challenge/source/src
$ python task2.py "Publ 527" 1994 2020
```
The program will show the output below (exemple):
```
$ MESSAGE: Running 2 processes
$ Download file: Publ 526 - 2020.pdf
$ Run processes
$ Download file: Publ 526 - 2019.pdf
$ Run processes
$ Downloads finished
```

### How to run unit tests - Task 2
```sh
$ cd challenge/source/
$ python -m unittest tests/test_task2.py
```
