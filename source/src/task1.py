
import sys
import json
import requests
import unittest
from bs4 import BeautifulSoup


def get_irs_tax(file_names):

    dic_output = {}
    per_page = 200
    start_index = 0

    for name in file_names:
        url = "https://apps.irs.gov/app/picklist/list/priorFormPublication.html"
        param = {
            'value': str(name),
            'criteria': 'formNumber',
            'submitSearch': 'Find',
            'resultsPerPage': per_page,
            'sortColumn': 'sortOrder',
            'indexOfFirstRow': start_index,
            'isDescending': 'false'
        }

        page = requests.get(url, param)
        if page.status_code == 200:

            # Html parser
            soup = BeautifulSoup(page.content, 'html.parser')

            # Get tables items
            # All the relevant content is on this tr tags
            tr_odd = soup.find_all('tr', class_='odd')
            tr_even = soup.find_all('tr', class_='even')

            for item in tr_even + tr_odd:
                # Get from_number from table
                form_number = item.find('td', class_='LeftCellSpacer').find('a').text.strip()

                # Get only exact name on search
                if form_number != name:
                    continue

                # Get from_title from table
                form_title = item.find('td', class_='MiddleCellSpacer').text
                form_title = form_title.replace('\n', '').replace('\t', '').strip()

                # Get from_year from table
                form_year = item.find('td', class_='EndCellSpacer').text
                form_year = form_year.replace('\n', '').replace('\t', '').strip()
                form_year = int(form_year)

                # Prepare the dic_output with the keys and values
                if form_number not in dic_output:
                    dic_output[form_number] = {
                        'form_number': form_number,
                        'form_title': form_title,
                        'min_year': 3000,
                        'max_year': 0
                    }

                # Update the max_year if necessary
                if form_year > dic_output[form_number]['max_year']:
                    dic_output[form_number]['max_year'] = form_year

                # Update the min_year if necessary
                if form_year < dic_output[form_number]['min_year']:
                    dic_output[form_number]['min_year'] = form_year
        else:
            print(f"Cannot find results for: {name}")

    print(json.dumps(list(dic_output.values())))
    return json.dumps(list(dic_output.values()))


if __name__ == '__main__':
    print('Results for the list of names: ', sys.argv[1:])
    get_irs_tax(sys.argv[1:])


