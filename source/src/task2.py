import os
import sys
import requests
import unittest
from bs4 import BeautifulSoup
from multiprocessing.pool import ThreadPool


def download_file(list_links):
    path, pdf_name, link = list_links
    with open(f"{path}/{pdf_name}", "wb") as file:
        # Get pdf file from link
        response = requests.get(link)

        if response.status_code == 200:
            # write file in the path
            print(f"Download file: {pdf_name}")
            file.write(response.content)


def run_downloader(process: int, list_links: list):
    """
    Inputs:
        process: (int) number of process to run
        images_url:(list) list of images url
    """
    print(f'MESSAGE: Running {process} processes')
    results = ThreadPool(process).imap_unordered(download_file, list_links)
    for r in results:
        print("Run process")


def download_irs_tax(params):
    file_name, min_year, max_year = params
    current_path = os.path.dirname(os.path.realpath(__file__))

    if min_year == "" or max_year == "" or file_name == "":
        raise Exception("All parameters are required")

    per_page = 200
    start_index = 0

    url = "https://apps.irs.gov/app/picklist/list/priorFormPublication.html"
    param = {
        'value': str(file_name),
        'criteria': 'formNumber',
        'submitSearch': 'Find',
        'resultsPerPage': per_page,
        'sortColumn': 'sortOrder',
        'indexOfFirstRow': start_index,
        'isDescending': 'false'
    }

    try:
        min_year = int(min_year)
        max_year = int(max_year)
    except ValueError:
        raise ValueError("Minimum year and maximum year must be integers")

    if max_year < min_year:
        raise ValueError("Minimum year must be lower than maximum year")
    else:

        # Make the request
        page = requests.get(url, param)

        # If there are results
        if page.status_code == 200:

            # Get page content
            soup = BeautifulSoup(page.content, 'html.parser')

            # Get tables items
            # All the relevant content is on this tr tags
            tr_odd = soup.find_all('tr', class_='odd')
            tr_even = soup.find_all('tr', class_='even')

            list_links = []
            for item in tr_even + tr_odd:

                # Get form number from table
                form_number = item.find('td', class_='LeftCellSpacer').find('a').text.strip()

                # Get only exact name on search
                if form_number != file_name:
                    continue

                # Get link to download file
                form_link = item.find('td', class_='LeftCellSpacer').find('a')['href']

                # Get year from table
                form_year_str = item.find('td', class_='EndCellSpacer').text
                form_year_str = form_year_str.replace('\n', '').replace('\t', '').strip()
                form_year = int(form_year_str)

                # If form_number is in the range [min,max]
                if int(min_year) <= form_year <= int(max_year):

                    # Create dir if not exist
                    path = f"{current_path}/{form_number}/"
                    if not os.path.exists(path):
                        os.makedirs(path)

                    # PDF name
                    pdf_name = f"{form_number} - {form_year_str}.pdf"
                    list_links.append((path, pdf_name, form_link))

            if len(list_links) > 0:
                # If the number of process is lower than number os files
                if len(list_links) < 10:
                    run_downloader(len(list_links), list_links)
                else:
                    run_downloader(10, list_links)
                print("Downloads finished")
            else:
                print("Nothing to download")
        else:
            print("There are no results")


if __name__ == '__main__':
    download_irs_tax(sys.argv[1:])
