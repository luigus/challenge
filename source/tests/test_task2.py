
import unittest
from src.task2 import download_irs_tax


class TestTask(unittest.TestCase):

    def test_empty_list(self):
        file_names = ("", "", "")
        with self.assertRaises(Exception):
            download_irs_tax(file_names)

    def test_change_years(self):
        file_names = ("Publ 1", "2020", "2019")
        with self.assertRaises(ValueError):
            download_irs_tax(file_names)

    def test_string_years(self):
        file_names = ("Publ 1", "asd", "2019")
        with self.assertRaises(ValueError):
            download_irs_tax(file_names)

    def test_string_years2(self):
        file_names = ("Publ 1", "2020", "asd")
        with self.assertRaises(ValueError):
            download_irs_tax(file_names)

    def test_string_years3(self):
        file_names = ("Publ 1", "asdasd", "asd")
        with self.assertRaises(ValueError):
            download_irs_tax(file_names)

    def test_valid(self):
        file_names = ("Publ 1", "2019", "2020")
        self.assertEqual(download_irs_tax(file_names), None)

    def test_valid2(self):
        file_names = ("Publ 1", "2014", "2020")
        self.assertEqual(download_irs_tax(file_names), None)


if __name__ == '__main__':
    unittest.main()
