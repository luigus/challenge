
import unittest
from src.task1 import get_irs_tax


class TestTask(unittest.TestCase):

    def test_empty_list(self):
        file_names = []
        self.assertEqual(get_irs_tax(file_names), '[]')

    def test_invalid(self):
        file_names = ["asdasd"]
        self.assertEqual(get_irs_tax(file_names), '[]')

    def test_invalid_list(self):
        file_names = ["asdasd", "ertertert", "sasdwerwe"]
        self.assertEqual(get_irs_tax(file_names), '[]')

    def test_valid(self):
        file_names = ["Publ 1"]
        self.assertNotEqual(get_irs_tax(file_names), '[]')

    def test_valid_list(self):
        file_names = ["Publ 1", "Publ 17"]
        self.assertNotEqual(get_irs_tax(file_names), '[]')

    def test_valid_same(self):
        file_names = ["Publ 17", "Publ 17"]
        self.assertNotEqual(get_irs_tax(file_names), '[]')


if __name__ == '__main__':
    unittest.main()
